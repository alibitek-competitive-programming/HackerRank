#include <bits/stdc++.h>

using namespace std;

void init()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
}

static int dummy = (init(), 0);

template<typename T, typename Container=std::deque<T> >
class iterable_queue : public std::queue<T,Container>
{
public:
    typedef typename Container::const_iterator const_iterator;

    const_iterator begin() const { return this->c.begin(); }                                                                               
    const_iterator end() const { return this->c.end(); }
};

void bfs(vector<vector<int>>& g, int v, int u, vector<int>& path)
{
    path.push_back(v);
           
    if (v == u)
    {
        return;
    }
    
    cout << "start: " << v << ", end: " << u << endl;
    
    for (int i = 0; i < g.size(); i++)
    {
        for (int j = 0; j < g[i].size(); j++)
        {
            cout << "node: " << i << " has adjacent neighbour: " << g[i][j] << endl;
        }
    }
    
    cout << endl;
    
    set<int> checked;
    queue<int> q;
    
    q.push(v);
    
    while (!q.empty())
    {
        int current = q.front();
        q.pop();
        
        for (const int & adjacentNode : g[current])
        {
            cout << "node: " << current << " has neighbour: " << adjacentNode << endl;
            const bool isChecked = checked.find(adjacentNode) != checked.end();
            if (!isChecked)
            {
                checked.insert(adjacentNode);
                q.push(adjacentNode);
                
                if (find(path.begin(), path.end(), adjacentNode) == path.end())
                    path.push_back(adjacentNode);
                //p[adjacentNode] = current;
            }
            
            if (adjacentNode == u)
            {
                breakFree = true;
                break;
            }
        }
        
        if (breakFree)
            break;
    }
    
    /*
    int i = u;
    path.push_back(i);
    
    while (1)
    {
        int pi = p[i];
        path.push_back(pi);
        
        if (pi == v)
            break;
        
        i = pi;
    }*/
    
    //reverse(path.begin(), path.end());
    
    path.pop_back();

    cout << "visited size: " << checked.size() << ", queue size: " << q.size() << endl;
    
    cout << endl;
} 

vector <int> skippingSubpathSum(int n, vector <int> c, vector < vector<int> > graph, vector <pair<int, int>> queries) 
{
    auto queriesSize = queries.size();
    
    vector<int> answers(queriesSize);
    
    for (int qid = 0; qid < queriesSize; ++qid) 
    {
        int u = queries[qid].first;
        int v = queries[qid].second;
        
        vector<int> path;
        bfs(graph, u, v, path);

        cout << "path: ";
        std::copy(path.begin(), path.end(), std::ostream_iterator<int>(std::cout, " "));
        cout << endl;
    }
    
    return answers;
}

/*
5                                                                                                                                                                                                                                                            
3 -2 1 -1 2                                                                                                                                                                                                                                                               
0 1                                                                                                                                                                                                                                                                       
1 2                                                                                                                                                                                                                                                                       
0 3
3 4
1
2 4

 */
int main() {
    int n;
    cin >> n;
    
    vector<int> c(n);
    for(int c_i = 0; c_i < n; c_i++)
    {
       cin >> c[c_i];
    }
    
    vector<vector<int>> graph(n);
    for (int i = 0; i < n-1; ++i) 
    {
        int u, v;
        cin >> u >> v;
        graph[u].push_back(v);
        graph[v].push_back(u);
    }
    
    int q;
    cin >> q;
    
    vector<pair<int, int>> queries;
    for (int i = 0; i < q; ++i) 
    {
        int u, v;
        cin >> u >> v;
        queries.push_back(make_pair(u, v));
    }
    
    vector <int> answers = skippingSubpathSum(n, c, graph, queries);
    for (ssize_t i = 0; i < answers.size(); i++) 
    {
        cout << answers[i] << (i != answers.size() - 1 ? "\n" : "");
    }

    cout << endl;


    return 0;
}
