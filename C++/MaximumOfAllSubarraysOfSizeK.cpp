// https://www.geeksforgeeks.org/sliding-window-maximum-maximum-of-all-subarrays-of-size-k/
// https://www.hackerrank.com/challenges/deque-stl/problem
#include <iostream>
#include <deque> 
using namespace std;

void printKMax(int arr[], int n, int k){
	// n = size of array
    // k = size of subarray
    deque<int> queue(k);

    // First subarray of length k
    int i;
    for (i = 0; i < k; ++i)
    {
        // Remove elements smaller than current element 
        while (!queue.empty() && arr[i] >= arr[queue.back()])
        {
            queue.pop_back();
        }

        // Add new element index at rear of queue
        queue.push_back(i);
    }

    // Next subarrays from k to n - 1
    while (i < n)
    {
        // Largest element of previous subarray is at the front of the queue
        printf("%d ", arr[queue.front()]);

        // Remove elements from previous subarrays
        while (!queue.empty() && (queue.front() <= (i - k)))
        {
            queue.pop_front();
        }

        // Remove elements smaller than current element
        while (!queue.empty() && (arr[i] >= arr[queue.back()]))
        {
            queue.pop_back();
        }

        // Add current element index to rear of queue
        queue.push_back(i);

        ++i;
    }

    // Print maximum element of last subarray
    printf("%d\n", arr[queue.front()]);
}

int main(){
  ios_base::sync_with_stdio(false);
	int t;
	cin >> t;
	while(t>0) {
		int n,k;
        cin >> n >> k;
    	int i;
    	int arr[n];
    	for(i=0;i<n;i++)
      		cin >> arr[i];
    	printKMax(arr, n, k);
    	t--;
  	}
  	return 0;
}

 
