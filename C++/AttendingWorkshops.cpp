// https://en.wikipedia.org/wiki/Interval_scheduling
// https://www.hackerrank.com/challenges/attending-workshops/problem

#include<bits/stdc++.h>

using namespace std;

//Define the structs Workshops and Available_Workshops.
//Implement the functions initialize and CalculateMaxWorkshops

struct Workshop
{
    int start_time;
    int duration;
    int end_time;

    bool operator<(const Workshop& other)
    {
        return end_time < other.end_time;
    }
};

struct Available_Workshops
{
    Available_Workshops()
        : n(0), 
          workshops(nullptr)
    {        
    }

    Available_Workshops(int n) 
        : n(n),
          workshops(new Workshop[n])
    {        
    }

    // the number of workshops the student signed up for
    int n;
    Workshop* workshops;
};

Available_Workshops* initialize(int start_time[], int duration[], int n)
{
    Available_Workshops* ptr = new Available_Workshops { n };
    ptr->n = n;

    for (int i = 0; i < n; ++i)
    {
       Workshop workShop { 
            start_time[i], 
            duration[i], 
            start_time[i] + duration[i] 
        };
        ptr->workshops[i] = workShop;
    }

    return ptr;
}

int CalculateMaxWorkshops(Available_Workshops* ptr)
{
    int count = 1;

    sort(ptr->workshops, ptr->workshops+ptr->n); 

    Workshop previousWorkshop = ptr->workshops[0];

    for (int i = 1; i < ptr->n; ++i)
    {
        Workshop currentWorkShop = ptr->workshops[i];

        if (currentWorkShop.start_time >= previousWorkshop.end_time)
        {                                                                          
            count++;                                                                
            previousWorkshop = currentWorkShop;                                         
        }     
    }

    return count;
}

int main(int argc, char *argv[]) {
    int n; // number of workshops
    cin >> n;
    // create arrays of unknown size n
    int* start_time = new int[n];
    int* duration = new int[n];

    for(int i=0; i < n; i++){
        cin >> start_time[i];
    }
    for(int i = 0; i < n; i++){
        cin >> duration[i];
    }

    Available_Workshops * ptr;
    ptr = initialize(start_time,duration, n);
    cout << CalculateMaxWorkshops(ptr) << endl;
    return 0;
}
