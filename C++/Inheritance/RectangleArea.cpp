#include <iostream>

using namespace std;
/*
 * Create classes Rectangle and RectangleArea
 */
 class Rectangle
 {
public:
    Rectangle()
        : m_width(0), m_height(0)
        {            
        }

    virtual void display() const
    {
        cout << m_width << " " << m_height << "\n";
    }     

protected:
        int m_width, m_height;
 };

 class RectangleArea : public Rectangle
 {
public:
        void read_input()
        {
            cin >> m_width >> m_height;
        }

        void display() const override
        {
            cout << m_width * m_height;
        }
 };


int main()
{
    /*
     * Declare a RectangleArea object
     */
    RectangleArea r_area;
    
    /*
     * Read the width and height
     */
    r_area.read_input();
    
    /*
     * Print the width and height
     */
    r_area.Rectangle::display();
    
    /*
     * Print the area
     */
    r_area.display();
    
    return 0;
} 
