// https://www.hackerrank.com/challenges/box-it/problem
#include<bits/stdc++.h>

using namespace std;

class Box
{
    public:
        Box()
            : m_length(0),
              m_breadth(0),
              m_height(0)
        {}

        Box(long long length, long long breadth, long long height)
            : m_length(length),
              m_breadth(breadth),
              m_height(height)
        {}

        Box(const Box& other)
            : m_length(other.m_length),
              m_breadth(other.m_breadth),
              m_height(other.m_height)
        {
        }

        long long getLength() const { return m_length; }
        long long getBreadth () const { return m_breadth; }
        long long getHeight () const { return m_height; }
        long long CalculateVolume() { return m_length * m_breadth * m_height; }

        bool operator<(const Box& other)
        {
            if (m_length < other.m_length)
            {
                return true;
            }

            if (m_length == other.m_length)
            {
                if (m_breadth < other.m_breadth)
                {
                    return true;
                }

                if (m_breadth == other.m_breadth)
                {
                    if (m_height < other.m_height)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        friend ostream& operator<<(ostream& stream, const Box& box);

    private:
        long long m_length, m_breadth, m_height;
};

ostream& operator<<(ostream& stream, const Box& box)
{
    stream << box.m_length << " " << box.m_breadth << " " << box.m_height;
    return stream;
}



void check2()
{
	int n;
	cin>>n;
	Box temp;
	for(int i=0;i<n;i++)
	{
		int type;
		cin>>type;
		if(type ==1)
		{
			cout<<temp<<endl;
		}
		if(type == 2)
		{
			int l,b,h;
			cin>>l>>b>>h;
			Box NewBox(l,b,h);
			temp=NewBox;
			cout<<temp<<endl;
		}
		if(type==3)
		{
			int l,b,h;
			cin>>l>>b>>h;
			Box NewBox(l,b,h);
			if(NewBox<temp)
			{
				cout<<"Lesser\n";
			}
			else
			{
				cout<<"Greater\n";
			}
		}
		if(type==4)
		{
			cout<<temp.CalculateVolume()<<endl;
		}
		if(type==5)
		{
			Box NewBox(temp);
			cout<<NewBox<<endl;
		}

	}
}

int main()
{
	check2();
}
