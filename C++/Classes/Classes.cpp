// https://www.hackerrank.com/challenges/c-tutorial-class/problem

#include <iostream>
#include <sstream>
using namespace std;

class Student
{
public:
    Student() :
        m_age(0), m_standard(0)
    {
    }

    int get_age() const
    {
        return m_age;
    }

    void set_age(int age)
    {
        if (m_age != age)
        {
            m_age = age;
        }
    }

    string get_first_name() const
    {
        return m_first_name;
    }

    void set_first_name(string first_name)
    {
        if (m_first_name != first_name)
        {
            m_first_name = first_name;
        }
    }


    string get_last_name() const
    {
        return m_last_name;
    }

    void set_last_name(string last_name)
    {
        if (m_last_name != last_name)
        {
            m_last_name = last_name;
        }
    }

    int get_standard() const
    {
        return m_standard;
    }

    void set_standard(int standard)
    {
        if (m_standard != standard)
        {
            m_standard = standard;
        }
    }

    string to_string() const
    {
        stringstream ss;
        ss << m_age << "," 
           << m_first_name << ","
           << m_last_name << ","
           << m_standard << "\n";
        return ss.str();
    }

private:
    int m_age;
    string m_first_name;
    string m_last_name;
    int m_standard;
};

int main() {
    int age, standard;
    string first_name, last_name;
    
    cin >> age >> first_name >> last_name >> standard;
    
    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);
    
    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();
    
    return 0;
}

 
