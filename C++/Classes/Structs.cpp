// https://www.hackerrank.com/challenges/c-tutorial-struct/problem

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

struct Student
{
    unsigned short age;
    string first_name;
    string last_name;
    int standard;

    Student() :
        age(0), standard(0)
    {    
    }

    friend ostream& operator<<(ostream& stream, const Student& x);
};

ostream& operator<<(ostream& stream, const Student& x)
{
    stream 
        << x.age << " " 
        << x.first_name << " " 
        << x.last_name << " " 
        << x.standard << "\n";

    return stream;
}

int main() {
    Student st;
    
    cin 
        >> st.age 
        >> st.first_name 
        >> st.last_name 
        >> st.standard;

    // cout << st.age << " " << st.first_name << " " << st.last_name << " " << st.standard;
    cout << st;
    
    return 0;
}

 
