#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>
using namespace std;

struct Attribute
{
  string name;
  string value;
};

struct Tag
{
  string name;
  vector<Attribute> attributes;
  bool isEnd = false;
};

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */  
    int N, // number of lines in the HRML source program 
        Q; // the number of queries
    cin >> N >> Q;
    
    cin.clear();
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    
    vector<Tag> tags;
        
    for (int i = 0; i < N; i++)
    {
      string line;
      getline(cin, line);  
      
      std::string::size_type length = line.length();
        
      if (!length)
      {
          continue;
      }
        
      Tag tag;
        
      // end tag
      if (line.compare(0, 2, "</") == 0)
      {
          // parse end tag name
          //tag.name = line.substr(2, length - 3);
          //tag.isEnd = true;
          true;
          //cout << "end tag: " << tag.name << endl;
      }
      else // start tag
      {
          // parse start tag name
          std::string::size_type found = line.find_first_of(' ', 1);
          if (found != std::string::npos)
          {
              tag.name = line.substr(1, found - 1);
              //cout << "start tag: " << tag.name << endl;
              
              // parse tag attributes
              string attributes = line.substr(found + 1, length - 3 - tag.name.length());
              string::size_type attrLength = attributes.length();
              //cout << "attributes: " << attributes << endl;
              
              std::string::size_type nextAttributePos = 0;
              
              do
              {
                std::string::size_type positionOfEquals = attributes.find(" = ", nextAttributePos);

                if (positionOfEquals != std::string::npos)
                {
                    Attribute attribute;
                    
                    attribute.name = attributes.substr(nextAttributePos, positionOfEquals - nextAttributePos);
                    
                    std::string::size_type lastQuote = attributes.find_first_of('"', positionOfEquals + 3 + 1);
                    attribute.value = attributes.substr(positionOfEquals + 3 + 1, lastQuote - 3 - attribute.name.length() - 1 - nextAttributePos);
                    
                    tag.attributes.push_back(attribute);
                    
                    //cout << setfill(' ') << setw(25) << "name: " << attribute.name << ", value: " << attribute.value << endl;
                    
                    nextAttributePos = attributes.find_first_not_of(' ', lastQuote + 2);
                    if (nextAttributePos == std::string::npos)
                        break;
                }
                else
                    break;
              }
              while (1);
          }
      }
      
      tags.push_back(tag);
    }
    
    for (int i = 0; i < Q; i++)
    {
        string query;
        getline(cin, query);
        //cout << "query: " << query << endl;
        
        string attributeSearchedFor;
        string tagSearchedFor;
        std::string::size_type queryLength = query.length();
        
        std::string::size_type foundTilda = query.find_last_of('~');
        if (foundTilda != std::string::npos)
        {
            attributeSearchedFor = query.substr(foundTilda + 1, queryLength - foundTilda - 1);
            
            //cout << "attributeSearchedFor: " << attributeSearchedFor << endl;
            
            string tagStr = query.substr(0, foundTilda);
            //cout << "tagStr: " << tagStr << endl;
            std::string::size_type lastDot = tagStr.find_last_of('.');
            if (lastDot != std::string::npos)
            {
                tagSearchedFor = tagStr.substr(lastDot + 1);
            }
            else
            {
                tagSearchedFor = tagStr;
            }
            //cout << "tagSearchedFor: " << tagSearchedFor << endl;
            
            vector<Tag>::iterator it = find_if(tags.begin(), tags.end(), [&tagSearchedFor](const Tag& tag) {
                return tag.name == tagSearchedFor;
            });
            
            string output;
            
            if (it != tags.end())
            {
                vector<Attribute>::iterator attributeIt = find_if(it->attributes.begin(), it->attributes.end(), [&attributeSearchedFor](const Attribute& attribute) {
                    return attribute.name == attributeSearchedFor;
                });
                
                if (attributeIt != it->attributes.end())
                {
                    output = attributeIt->value;
                }
                else
                {
                    output = "Not Found!";
                }
            }
            else
            {
                output = "Not Found!";
            }
            
            cout << output << endl;
        }
        else
        {
            cout <<  "Not Found!" << endl;
        }
    }
    
    return 0;
}
 
