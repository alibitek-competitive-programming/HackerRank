// https://www.hackerrank.com/challenges/prettyprint/problem
#include <iostream>
#include <iomanip> 
using namespace std;

int main() {
	int T; cin >> T;
	cout << setiosflags(ios::uppercase);
	cout << setw(0xf) << internal;
	while(T--) {
		double A; cin >> A;
		double B; cin >> B;
		double C; cin >> C;

		/* Enter your code here */
        // A : Strip its decimal (i.e., truncate it) and print its hexadecimal representation (including the prefix) in lower case letters.
        cout << nouppercase
             << setw(0) 
             << hex
             << showbase
             << (long long)A << '\n';

        // B: Print it to a scale of decimal places, preceded by a + or - sign (indicating if it's positive or negative), right justified, and left-padded with underscores so that the printed result is exactly 15 characters wide.
        cout << dec
             << setw(0xf) << internal
             << right
             << setfill('_')
             << showpos
             << fixed
             << setprecision(2)
             << B << '\n';

        // C: Print it to a scale of exactly nine decimal places, expressed in scientific notation using upper case.
        cout << setw(0) 
             << setprecision(9)
             << noshowpos
             << scientific
             << uppercase
             << C << '\n';
	}
	return 0;

} 
