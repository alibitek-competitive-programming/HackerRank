// https://www.hackerrank.com/challenges/cpp-variadics/problem
// https://en.cppreference.com/w/cpp/language/parameter_pack

#include <iostream>
#include <cmath>

using namespace std;

// Enter your code for reversed_binary_value<bool...>()

template <bool... binary_digits>
int reversed_binary_value()
{
    const size_t size = sizeof...(binary_digits);
    bool bits[size] = { binary_digits... };

    int result = 0;
    for (int i = size - 1; i >= 0; --i)
    {
        if (bits[i])
        {
            result += pow(2, i);
        }
    }

    return result;
}


template <int n, bool...digits>
struct CheckValues {
  	static void check(int x, int y)
  	{
    	CheckValues<n-1, 0, digits...>::check(x, y);
    	CheckValues<n-1, 1, digits...>::check(x, y);
  	}
};

template <bool...digits>
struct CheckValues<0, digits...> {
  	static void check(int x, int y)
  	{
    	int z = reversed_binary_value<digits...>();
    	std::cout << (z+64*y==x);
  	}
};

int main()
{
  	int t; std::cin >> t;

  	for (int i=0; i!=t; ++i) {
		int x, y;
    	cin >> x >> y;
    	CheckValues<6>::check(x, y);
    	cout << "\n";
  	}
}
