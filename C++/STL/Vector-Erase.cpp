#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

/* 1 4 6 2 8 9
   0 1 2 3 4 5
   ^           ^
begin          end

v.erase(v.begin() + 2);
    1 6 2 8 9
    0 1 2 3 4
    ^
  begin

  should remove: 6 and 8
  
start_pos=2 
end_pos=4
v.erase(v.begin() + start_pos - 1, begin() + end_pos - 1)
*/

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int N;
    cin >> N;
    
    vector<int> v(N);
    
    for (int i = 0; i < N; ++i)
    {
        cin >> v[i];
    }
    
    int posToErase;
    cin >> posToErase;
    
    // remove elements in the range
    int rangeBegin, rangeEnd;
    cin >> rangeBegin >> rangeEnd;
    
    v.erase(v.begin() + posToErase - 1);
    
    v.erase(v.begin() + rangeBegin - 1, v.begin() + rangeEnd - 1);
    
    cout << v.size() << "\n";
    copy(begin(v), end(v), ostream_iterator<int>(cout, " "));
    
    return 0;
}
 
