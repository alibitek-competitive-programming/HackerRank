#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int Q;
    cin >> Q;
    
    map<string, int> m;
    
    for (int i = 0; i < Q; i++)
    {
        int type, mark;
        string name;
        
        cin >> type;
        
        switch (type)
        {
            case 1:
            {
                cin >> name >> mark;
                auto tmp = m[name];
                tmp += mark;
                m[name] = tmp;
                break;
            }
                
            case 2:
                cin >> name;
                m.erase(name);
                break;
                
            case 3:
                cin >> name;
                
                auto it = m.find(name);
                
                if ( it != m.end() )
                {
                    cout << it->second << endl;
                }
                else
                {
                    cout << 0 << endl;
                }
                
                break;
        }
    }
    
    return 0;
}
