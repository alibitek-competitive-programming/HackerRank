#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <set>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int Q;
    cin >> Q;
    
    set<int> s;
    
    for (int q = 0; q < Q; q++)
    {
        int y, x;
        cin >> y >> x;
        
        switch (y)
        {
            case 1:
                s.insert(x);
                break;
                
            case 2:
                s.erase(x);
                break;
                
            case 3:
                set<int>::iterator it = s.find(x);
                if (it != s.end())
                {
                    cout << "Yes" << "\n";
                }
                else
                {
                    cout << "No" << "\n";
                }
                
                break;
        }
    }
    
    return 0;
}
