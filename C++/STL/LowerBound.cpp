#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

// 10 10 10 21 22 23 30 30
// 0  1   2  3  4  5  6  7

// lower_bound gives back iterator to first value  >= val
// upper_bound gives back iterator to first value  > val
// aka a range: [lower_bound, upper_bound)

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    int N;
    cin >> N;
    
    vector<int> v(N);
    
    for (int i = 0; i < N; ++i)
    {
        cin >> v[i];
    }
    
    int Q;
    cin >> Q;
    
    for (int i = 0; i < Q; ++i)
    {
        int query;
        cin >> query;
        
        vector<int>::iterator low = lower_bound(v.begin(), v.end(), query);
        
        if (v[low - v.begin()] == query)
        {
            cout << "Yes " << (low - v.begin() + 1) << endl;
        }
        else
        {
            cout << "No " << (low - v.begin() + 1) << endl;
        }
        
        /*
        int idx = 0;
        auto it = find_if(begin(v), end(v), [&query, &idx](const int e) {
            if (e == query)
            {
                return true;
            }
            
            ++idx;
            
            return false;
        });
        
        if (it != v.end())
        {
            cout << "Yes " << idx + 1 << endl;
        }
        else
        {
            query = *lower_bound(v.begin(), v.end(), query);
            idx = 0;
            
            auto it2 = find_if(begin(v), end(v), [&query, &idx](const int e) {
                if (e == query)
                {
                    return true;
                }
                
                ++idx;
                
                return false;
            });
            
            cout << "No " << idx + 1 << endl;
        }*/
    }
    
    return 0;
}
