#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
using namespace std;


int function(int x)
{
    return x;
}

bool is_one_to_one(const vector<int>& xs, const vector<int>& ys)
{
    vector<int> seen(xs.size());
    
    for (vector<int>::size_type i = 0; i < ys.size(); i++)
    {                       
        for (vector<int>::size_type j = 0; j < xs.size(); j++)
        {                                        
            // each element in the codomain is the image of a single element in the domain
            if (ys[i] == xs[j])
            {
                seen[j]++;
            }
        }            
    }
    
    for (vector<bool>::size_type i = 0; i < seen.size(); i++)
    {
        if (seen[i] > 1)
            return false;
    }
    
    return true;
}

bool is_onto(const vector<int>& xs, const vector<int>& ys)
{    
    // A function  is onto if and only if each element in the co-domain  
    // codomain elements
    for (vector<int>::size_type i = 0; i < ys.size(); i++)
    {        
        bool y_is_image_of_x = false;
     
        // is the image of, at least, one element in the domain
        for (vector<int>::size_type j = 0; j < xs.size(); j++)
        {
            if (ys[i] == xs[j])
            {
                y_is_image_of_x = true;
                break;
            }
        }
        
        if (!y_is_image_of_x)
            return false;
    }
    
    return true;
}

bool is_bijective(const vector<int>& xs, const vector<int>& ys)
{
    return is_one_to_one(xs, ys) && is_onto(xs, ys);
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int n;
    cin >> n;
    
    if (n < 1 || n > 20) 
    {
        cout << "NO";
        return 0;
    }
    
    string line;
    cin>>ws;
    getline(cin, line);    
    
    istringstream stream(line);
    int y;
    
    vector<int> ys;
    ys.reserve(n);
    
    while (stream >> y)
    {
        ys.push_back(y);
    }
    
    
    
    vector<int> xs;
    xs.reserve(n);
    
    for (vector<int>::size_type j = 0; j < ys.size(); j++)
    {               
        //cout << j + 1;
            xs.push_back(j+1);
    }    

    if (is_one_to_one(xs, ys))
        cout << "YES";
    else
        cout << "NO";
    
    
    return 0;
}
 
