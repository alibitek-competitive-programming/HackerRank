#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_map>
using namespace std;

int calculateClimbingWays(int n)
{
    static unordered_map<int, int> memo;
    
    if (n < 0)
        return 0;
    
    if (n == 0)
        return 1;
    
    if (memo.find(n) == memo.end())
    {
        memo[n] = calculateClimbingWays(n - 1) + calculateClimbingWays(n - 2) + calculateClimbingWays(n - 3);
    }
    
    return memo[n];
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int s; //  the number of staircases
    cin >> s;
    
    // the height of staircase
    int n;
    
    for (int i = 0; i < s; i++)
    {
        cin >> n;
        
        cout << calculateClimbingWays(n) << "\n";
    }
    
    cout << flush;
    
    return 0;
}
