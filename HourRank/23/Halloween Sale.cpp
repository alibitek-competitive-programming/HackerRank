#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int p, // the first games' price
        d, // every subsequent games' price will be d dollars less than the cost of the previous game
        m, // the price will be d until d <= m, after which the price of every game will be m dollars
        s; // the number of dollars in the wallet
        
    cin >> p >> d >> m >> s;
    
    // How many games can you buy during the Halloween Sale?
    int numberOfGames = 0;
    
    if (s >= p && (1 <= m && m <= p && p <= 100))
    {
        // buy first game at p dollars
        int cost = p;
        s -= cost;
        //cout << "cost ~ p: " << cost << '\n';
        ++numberOfGames;
        
        // buy games at d dollars less each until the cost becomes <= m
        while (cost - d > m && s >= cost - d) {
            //cout << "cost ~ d: " << cost - d << '\n';
            cost -= d;
            s -= cost;
            ++numberOfGames;
        }
        
        while (s >= cost) {
            cost = m;
            //cout << "cost ~ m: " << cost << '\n';
            s -= cost;
            ++numberOfGames;
        }
    }
    
    cout << numberOfGames << '\n';
    
    return 0;
}
