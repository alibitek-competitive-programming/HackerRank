#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits>

using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int n;
    cin >> n;
    cin.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' ); 
    
    vector<string> strings(n);
    for ( int i = 0; i < n; i++ )
        getline ( cin, strings[i] );
    
    int q;
    cin >> q;
    cin.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' ); 
    
    vector<string> queries(q);
    for ( int i = 0; i < q; i++ )
        getline ( cin, queries[i] );
    
    for_each ( queries.begin(), queries.end(), [&strings] ( const auto& query )
        {
            cout << count ( strings.begin(), strings.end(), query ) << "\n";
        } 
    );
    
    return 0;
}
