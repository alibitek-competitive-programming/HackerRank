#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main() 
{
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int N, // number of sequences 
        Q; // number of queries
    cin >> N >> Q;

    typedef vector< vector<int> > TMat;
    TMat seqList(N, vector<int>());
    
    int lastAnswer = 0;
    
    int queryType, x, y;
    
    for (int nq = 0 ; nq < Q; nq++)
    {
        cin >> queryType >> x >> y;
        
        int index = (x ^ lastAnswer) % N;
        TMat::reference seq = seqList[index];
        
        switch (queryType)
        {
            case 1:
                seq.push_back(y);
                break;
                
            case 2:
                lastAnswer = seq[y % seq.size()];
                cout << lastAnswer << "\n";
                break;
        }
    }
    
    return 0;
}
