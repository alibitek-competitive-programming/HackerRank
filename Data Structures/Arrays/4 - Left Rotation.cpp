#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
using namespace std;

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int n, // the number of integers
        d; // the number of left rotations you must perform
    cin >> n >> d;
    
    vector<int> v((istream_iterator<int>(cin)), istream_iterator<int>());
    
    for (int i = 0; i < n; ++i)
    {
        cout << v[(i + d) % n] << " ";
    }
    
    /*
    vector<int> arr(n);
    
    for (int i = 0; i < n; i++)
        cin >> arr[i];
    
    for (int di = 0; di < d; di++)
    {
        for (int i = 0; i < n - 1; i++)
        {
            //iter_swap(begin(arr) + i + 1, begin(arr) + i);
            swap(arr[i + 1], arr[i]);
        }
    }
    
     for (int i = 0; i < n; i++)
    {
        if (i < n - 1)
        {
            cout << arr[i] << " ";
        }
        else
        {
            cout << arr[i];
        }
    }
    
    */
    
    return 0;
}
