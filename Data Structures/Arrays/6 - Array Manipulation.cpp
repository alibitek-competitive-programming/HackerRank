#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <numeric>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    long n;
    cin >> n;
    vector<long> list(n, 0);
    
    long m;
    cin >> m;
    
    /*
     Instead of storing the actual values in the array, you store the difference between the current element and the previous element. 
     
     So you add sum to a[p] showing that a[p] is greater than its previous element by sum. 
     You subtract sum from a[q+1] to show that a[q+1] is less than a[q] by sum (since a[q] was the last element that was added to sum). 
     
     By the end of all this, you have an array that shows the difference between every successive element. 
     
     By adding all the positive differences, you get the value of the maximum element
    */
    
    for (int i = 0; i < m; i++)
    {
        int a, b, k;
        cin >> a >> b >> k;
        
        a -= 1;
        b -= 1;
        
        /*
        transform(begin(list) + a, begin(list) + b + 1, // input
                  begin(list) + a, // output
                  [k](int e) { return e + k; });
                  
        
        For every input line of a-b-k, you are given the range (a to b) where the values increase by k. 
        So instead of keeping track of actual values increasing, just keep track of the rate of change (i.e. a slope) in terms of where the rate started its increase and where it stopped its increase. 
        This is done by adding k to the "a" position of your array and adding -k to the "b+1" position of your array for every input line a-b-k, and that's it. 
        "b+1" is used because the increase still applied at "b".
        */
        list[a] += k;
        
        if ((b + 1) < n)
            list[b + 1] -= k;
    }
    
    //cout << *max_element(cbegin(list), cend(list));
    
    /*
     * The maximum final value is equivalent to the maximum accumulated "slope" starting from the first position, because it is the spot which incremented more than all other places. 
     * Accumulated "slope" means to you add slope changes in position 0 to position 1, then add that to position 2, and so forth, looking for the point where it was the greatest
     */
    long max = 0, sum = 0;
    for (int i = 0; i < n; i++)
    {
       sum = sum + list[i];     
        
       if(max < sum) 
           max = sum;
    }
    
    cout << max;
    
    return 0;
}
