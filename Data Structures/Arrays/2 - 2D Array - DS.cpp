#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

int main()
{
    vector< vector<int> > arr(6, vector<int>(6));
    
    for(int arr_i = 0; arr_i < 6; arr_i++)
    {
       for(int arr_j = 0; arr_j < 6; arr_j++)
       {
          cin >> arr[arr_i][arr_j];
       }
    }
    
    int maxHourGlass = numeric_limits<int>::min();
    //vector<int> hourGlasses(16);
    
    for(int arr_i = 0; arr_i <= 3; arr_i++)
    {       
       for (int arr_j = 0; arr_j <= 3; arr_j++)
       {
          int hourglass = arr[arr_i][arr_j];
          hourglass += arr[arr_i][arr_j + 1];
          hourglass += arr[arr_i][arr_j + 2];
          
          hourglass += arr[arr_i + 1][arr_j + 1];
          
          hourglass += arr[arr_i + 2][arr_j];
          hourglass += arr[arr_i + 2][arr_j + 1];
          hourglass += arr[arr_i + 2][arr_j + 2];
          
          //hourGlasses.push_back(hourglass);
          
          if (hourglass > maxHourGlass)
              maxHourGlass = hourglass;
       }
       
    }

    //cout << *max_element(cbegin(hourGlasses), cend(hourGlasses));
    cout << maxHourGlass;
    
    return 0;
}
 
