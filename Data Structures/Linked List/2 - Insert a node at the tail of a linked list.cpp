#include <iostream>

/*
  Insert Node at the end of a linked list 
  head pointer input could be NULL as well for empty list
  Node is defined as 
 
*/

struct Node
{
    int data;
    struct Node *next;
};
  
Node* Insert(Node *head, int data)
{
  // Complete this method
    
    Node *node = new Node;
    node->data = data;
    node->next = nullptr;
    
    if (!head)
    {
        head = node;
    }
    else
    {
        Node* current = head;
        
        while (current->next != nullptr)
        {
            current = current->next;
        }
        
        current->next = node;
    }
    
    return head;
}

int main()
{
    Node* head = nullptr;

    head = Insert(head, 2);
    std::cout << head->data;
    
    Node* n1 = new Node;
    n1->data = 2;
    n1->next = nullptr;
    head = n1;
    
    head = Insert(head, 3);
    std::cout << head->data;
    
    return 0;
}
