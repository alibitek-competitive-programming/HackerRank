#include <iostream>

/*
  Insert Node at a given position in a linked list 
  head can be NULL 
  First element in the linked list is at position 0
*/
struct Node
{
    int data;
    struct Node *next;
};
  
Node* InsertNth(Node *head, int data, int position)
{
  // Complete this method
    
    Node *node = new Node;
    node->data = data;
    node->next = nullptr;
    
    if (!head)
    {
        head = node;
    }
    else
    {
        Node* current = head;
        
        if (position == 0)
        {
            node->next = head;
            head = node;
        }
        else
        {
            while (position - 1 > 0)
            {
                current = current->next;
                position--;
            }
            
            node->next = current->next;
            current->next = node;
        }
    }
    
    return head;
}

int main()
{
    Node* head = nullptr;

    head = InsertNth(head, 3, 0);
    std::cout << head->data << "\n";
    
    Node* n1 = new Node;
    n1->data = 3;
    n1->next = nullptr;
    head = n1;
    
    Node* n2 = new Node;
    n2->data = 5;
    n2->next = nullptr;
    n1->next = n2;
    
    Node* n3 = new Node;
    n3->data = 6;
    n3->next = nullptr;
    n2->next = n3;
    
    head = InsertNth(head, 4, 2);
    std::cout << head->data << "\n";
    
    
    Node* current = head;
    do
    {
        std::cout << current->data << " ";
    }
    while ((current = current->next) != nullptr);
    
    return 0;
}
