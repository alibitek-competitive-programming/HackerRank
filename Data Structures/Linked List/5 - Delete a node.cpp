#include <iostream>

/*
  Delete Node at a given position in a linked list 
*/
struct Node
{
    int data;
    struct Node *next;
};
  
Node* Delete(Node *head, int position)
{
  // Complete this method
   if (!head)
       return head;
   
   if (position == 0)
   {
       Node* tmp = head;
       head = head->next;
       delete tmp;
   }
   else
   {
       Node* current = head;
       while (position - 1 > 0)
       {
           current = current->next;
           position--;
       }
       
       Node* nodetoDelete = current->next;
       current->next = current->next->next;
       delete nodetoDelete;
   }
   
   return head;
}

int main()
{
    Node* head = nullptr;
    
    Node* x = new Node;
    x->data = 1;
    x->next = nullptr;
    head = x;
    head = Delete(head, 0);
    std::cout << std::hex << head << "\n";
    
    Node* n1 = new Node;
    n1->data = 1;
    n1->next = nullptr;
    head = n1;
    
    Node* n2 = new Node;
    n2->data = 2;
    n2->next = nullptr;
    n1->next = n2;
    
    Node* n3 = new Node;
    n3->data = 3;
    n3->next = nullptr;
    n2->next = n3;
    
    head = Delete(head, 0);
    std::cout << head->data << "\n";
    
    Node* current = head;
    do
    {
        std::cout << current->data << " ";
    }
    while ((current = current->next) != nullptr);
    
    return 0;
}
 
