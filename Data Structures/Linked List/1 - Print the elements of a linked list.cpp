#include <iostream>
#include <cstddef>
/*
  Print elements of a linked list on console 
  head pointer input could be NULL as well for empty list
  Node is defined as 
  struct Node
  {
     int data;
     struct Node *next;
  }
*/
struct Node
{
    int data;
    struct Node *next;
};

void Print(Node *head)
{
  // This is a "method-only" submission. 
  // You only need to complete this method. 
    if (head)
    {
        Node* current = head;
        do
        {
            std::cout << current->data << "\n";
            current = current->next;
        } while (current);
    }
}
 
int main()
{
    Node* head = NULL;
    Print(head);
    
    Node* n3 = new Node;
    n3->data = 3;
    n3->next = NULL;
    
    Node* n2 = new Node;
    n2->data = 2;
    n2->next = n3;
    
    Node* n1 = new Node;
    n1->data = 1;
    n1->next = n2;

    head = n1;
    Print(head);
    
    delete n3;
    delete n2;
    delete n1;
}
