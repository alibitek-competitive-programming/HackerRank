#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

/*
 Print elements of a linked list in reverse order as standard output
  head pointer could be NULL as well for empty list
*/
struct Node
{
    int data;
    struct Node *next;
};
  
Node* Reverse(Node *head)
{
  // Complete this method
    if (!head || !head->next)
        return head;

    Node* newHead = Reverse(head->next);
    head->next->next = head;
    head->next = nullptr;
    
    return newHead;
}

int main()
{
    Node* head = nullptr;
    
    head = Reverse(head);
    std::cout << std::hex << head << "\n";
    
    Node* x = new Node;
    x->data = 1;
    x->next = nullptr;
    head = x;
    
    head = Reverse(head);
    std::cout << head->data << "\n";
    
    Node* n1 = new Node;
    n1->data = 1;
    n1->next = nullptr;
    head = n1;
    
    Node* n2 = new Node;
    n2->data = 2;
    n2->next = nullptr;
    n1->next = n2;
    
    Node* n3 = new Node;
    n3->data = 3;
    n3->next = nullptr;
    n2->next = n3;
    
    head = Reverse(head);
    std::cout << head->next->data << "\n";
    
    return 0;
}
 
 
 
