#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

/*
 Print elements of a linked list in reverse order as standard output
  head pointer could be NULL as well for empty list
*/
struct Node
{
    int data;
    struct Node *next;
};
  
int CompareLists(Node *headA, Node* headB)
{
  // Complete this method
    if (!headA && !headB)
        return 1;
    else if (!headA || !headB)
        return 0;
    
    Node* a = headA, *b = headB;
    
    while ( (a != nullptr) 
         && (b != nullptr)
         && a->data == b->data
         )
    {
        a = a->next;
        b = b->next;
    }
    
    // last node -> next -> null
    return a == b ? 1 : 0;
}

int main()
{
    Node* l0 = nullptr;
    
    Node* l1 = new Node;
    l1->data = 1;
    l1->next = nullptr;
    
    int res = CompareLists(l0, l1);
    std::cout << res << "\n";
    
    Node* l32 = new Node;
    l32->data = 2;
    l32->next = nullptr;
    
    Node* l31 = new Node;
    l31->data = 1;
    l31->next = l32;
    
    Node* l34 = new Node;
    l34->data = 2;
    l34->next = nullptr;
    
    Node* l35 = new Node;
    l35->data = 1;
    l35->next = l34;
    
    res = CompareLists(l31, l35);
    std::cout << res << "\n";
    
    return 0;
}
 
 
 
 
