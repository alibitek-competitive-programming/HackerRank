#include <iostream>

/*
  Insert Node at the begining of a linked list
  Initially head pointer argument could be NULL for empty list
  return back the pointer to the head of the linked list in the below method.
*/

struct Node
{
    int data;
    struct Node *next;
};
  
Node* Insert(Node *head, int data)
{
  // Complete this method
    
    Node *node = new Node;
    node->data = data;
    node->next = nullptr;
    
    if (!head)
    {
        head = node;
    }
    else
    {
        node->next = head;
        head = node;
    }
    
    return head;
}

int main()
{
    Node* head = nullptr;

    head = Insert(head, 1);
    std::cout << head->data;
    
    Node* n1 = new Node;
    n1->data = 1;
    n1->next = nullptr;
    head = n1;
    
    head = Insert(head, 2);
    std::cout << head->data;
    
    return 0;
}
