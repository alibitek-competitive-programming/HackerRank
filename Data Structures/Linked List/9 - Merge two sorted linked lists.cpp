#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

/*
  Merge two sorted lists A and B as one linked list
*/

struct Node
{
    int data;
    struct Node *next;
};

Node* MergeLists(Node *headA, Node* headB)
{
  // This is a "method-only" submission. 
  // You only need to complete this method 
    if (!headA && !headB)
        return nullptr;
    
    if (headA && !headB)
        return headA;
    
    if (headB && !headA)
        return headB;
    
    
}

int main()
{
    Node* a4 = new Node;
    a4->data = 6;
    a4->next = nullptr;

    Node* a3 = new Node;
    a3->data = 5;
    a3->next = a4;

    Node* a2 = new Node;
    a2->data = 3;
    a2->next = a3;

    Node* a1 = new Node;
    a1->data = 1;
    a1->next = a2;
    
    Node* b3 = new Node;
    b3->data = 7;
    b3->next = nullptr;
    
    Node* b2 = new Node;
    b2->data = 4;
    b2->next = b3;
    
    Node* b1 = new Node;
    b1->data = 2;
    b1->next = b2;

    Node* head = MergeLists(a1, b1);
    std::cout << head->data;
    
    return 0;
}
