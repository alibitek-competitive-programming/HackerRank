#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

struct Node
{
    int data;
    struct Node *next;
};

int GetNode(Node *head, int positionFromTail)
{
  // This is a "method-only" submission. 
  // You only need to complete this method. 
    if (!head || !head->next)
        return head->data;
    
   Node* current = head;
   Node* result = head;
   
   int pos = 0;
   while (current != nullptr)
   {
       current = current->next;
       
       if (pos > positionFromTail)
           result = result->next;
       
       pos++;
   }
    
   return result->data;
}

int main()
{
    Node* l32 = new Node;
    l32->data = 6;
    l32->next = nullptr;
    
    Node* l31 = new Node;
    l31->data = 5;
    l31->next = l32;
    
    Node* l30 = new Node;
    l30->data = 3;
    l30->next = l31;

    std::cout << GetNode(l30, 0);
    
    return 0;
}
