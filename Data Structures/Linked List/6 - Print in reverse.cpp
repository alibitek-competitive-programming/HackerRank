#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

/*
 Print elements of a linked list in reverse order as standard output
  head pointer could be NULL as well for empty list
*/
struct Node
{
    int data;
    struct Node *next;
};
  
void ReversePrint(Node *head)
{
  // This is a "method-only" submission. 
  // You only need to complete this method. 
    if (head)
    {
        std::vector<int> printOrder;
        Node* current = head;
        
        do
        {
            printOrder.push_back(current->data);
        } while ( (current = current->next) != nullptr );
        
        std::copy(printOrder.rbegin(), printOrder.rend(), std::ostream_iterator<int>(std::cout, "\n"));
    }
}

void ReversePrintRecursive(Node* head)
{
    if (head)
    {
        ReversePrintRecursive(head->next);
        std::cout << head->data << "\n";
    }
}

int main()
{
    Node* head = nullptr;
    
    Node* x = new Node;
    x->data = 1;
    x->next = nullptr;
    head = x;
    
    ReversePrint(head);
    
    Node* n1 = new Node;
    n1->data = 1;
    n1->next = nullptr;
    head = n1;
    
    Node* n2 = new Node;
    n2->data = 2;
    n2->next = nullptr;
    n1->next = n2;
    
    Node* n3 = new Node;
    n3->data = 3;
    n3->next = nullptr;
    n2->next = n3;
    
    ReversePrintRecursive(head);
    
    return 0;
}
 
 
