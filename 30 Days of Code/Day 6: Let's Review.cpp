#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int T;
    cin >> T;
    
    cin.clear();
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    
    string line;
    for (int i = 0; i < T; i++) 
    {
        getline(cin, line);
        
        ostringstream even, odd;
        
        auto lineSize = line.length();       
        for (int j = 0; j < lineSize; j++) {
            if (j % 2 == 0) {
                even << line[j];
            }
            else
            {
                odd << line[j];
            }
        }
        
        cout << even.str() << " " << odd.str() << endl;
    }
    
    return 0;
}
 
