#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int returnDay, returnMonth, returnYear;
    cin >> returnDay >> returnMonth >> returnYear;
    
    int dueDay, dueMonth, dueYear;
    cin >> dueDay >> dueMonth >> dueYear;
    
    long fine = 0;
    
    if (returnYear == dueYear) {
        if (returnMonth == dueMonth) {
            if (returnDay > dueDay) {
                fine = 15 * (returnDay - dueDay);
            }
        } else if (returnMonth > dueMonth) {
            fine = 500 * (returnMonth - dueMonth);
        }
    } else if (returnYear > dueYear) {
        fine = 10000;
    }
    
    cout << fine << endl;
    
    return 0;
}
