#include <iostream>

using namespace std;

int main() {
    cout 
         // Number of lectures
         << "5" << '\n'

         // Canceled - YES
         << "5 3" << '\n'
         << "-1 90 999 100 0" << '\n'
        
         // Canceled - NO
         << "4 2" << '\n' 
         << "0 -1 2 1" << '\n'
         
         // Canceled - YES
         << "3 3" << '\n' 
         << "-1 0 1" << '\n'
         
         // Canceled - NO
         << "6 1" << '\n' 
         << "-1 0 1 -1 2 3" << '\n'
         
         // Canceled - YES
         << "7 3" << '\n' 
         << "-1 0 1 2 3 4 5" << '\n';
    
    return 0;
}
