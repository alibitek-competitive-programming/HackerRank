#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

bool isPrime(long n) {
    if (n == 2)
        return true;
    else if (n <= 1 // 1 or negative numbers are not prime
            || (n & 1) == 0) // even numbers > 2 are not prime
        return false;
    
    // Check for primality using odd numbers from 3 to sqrt(n)
    for (int i = 3; i <= sqrt(n); i += 2) {
        // // n is not prime if it is evenly divisible by some 'i' in this range
        if ( n % i == 0 )
            return false;
    }
    
    return true;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int T;
    cin >> T;
    
    while (T-- > 0) {
        long n;
        cin >> n;
        
        cout << (isPrime(n) ? "Prime" : "Not prime") << endl;
    }
    
    return 0;
}
