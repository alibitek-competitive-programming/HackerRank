#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    double mealCost; // the cost of the meal before tax and tip
    int tipPercent; // the percentage of mealCost being added as tip
    int taxPercent; // the percentage of mealCost being added as tax
    
    cin >> mealCost >> tipPercent >> taxPercent;
    
    double tip = mealCost * (tipPercent / 100.0);
    double tax = mealCost * (taxPercent / 100.0);
    int totalCost = round(mealCost + tip + tax); 

    cout << "The total meal cost is " << totalCost << " dollars.";

    return 0;
}
