#include <iostream>
#include <cstddef>
#include <queue>
#include <string>
#include <cstdlib>

using namespace std;

class Node {
    public:
        int data;
        Node *left,*right;
        Node(int d) {
            data=d;
            left=right=NULL;
        }
};

class Solution {
    public:
  		Node* insert(Node* root, int data) {
            if(root == NULL){
                return new Node(data);
            } else {
                Node* cur;
                
                if (data <= root->data) {
                    cur=insert(root->left,data);
                    root->left=cur;
                } else {
                   cur=insert(root->right,data);
                   root->right=cur;
                }
                
                return root;
           }
        }
    
    // Because a level-order traversal goes level-by-level, it's also known as a breadth-first-search (BFS).
	void levelOrder(Node *root) {
      //Write your code here
        if (root) {
            queue<Node*> visitedNodes;
            
            visitedNodes.push(root);
            
            while (!visitedNodes.empty()) {
                Node* visitedNode = visitedNodes.front();
                visitedNodes.pop();
                
                cout << visitedNode->data << " ";
                
                if (visitedNode->left) {
                    visitedNodes.push(visitedNode->left);
                }
                
                if (visitedNode->right) {
                    visitedNodes.push(visitedNode->right);
                }
            }
        }
	}
}; //End of Solution

int main() {
    Solution myTree;
    Node* root = NULL;
    
    int T,data;
    cin >> T;
    
    while(T-- > 0) {
        cin >> data;
        root = myTree.insert(root, data);
    }
    
    myTree.levelOrder(root);
    
    return 0;
}
    
