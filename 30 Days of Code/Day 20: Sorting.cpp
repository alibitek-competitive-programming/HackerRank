#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void bubbleSort(vector<int>& a, size_t n)
{
    int totalNumberOfSwaps = 0;
    
    for (unsigned i = 0; i < n; i++) 
    {
        // Track number of elements swapped during a single array traversal
        int numberOfSwaps = 0;
    
        for (unsigned j = 0; j < n - 1; j++) 
        {
            // Swap adjacent elements if they are in decreasing order
            if (a[j] > a[j + 1]) 
            {
                swap(a[j], a[j + 1]);
                ++numberOfSwaps;
                ++totalNumberOfSwaps;
            }
        }
    
        // If no elements were swapped during a traversal, array is sorted
        if (numberOfSwaps == 0) {
            break;
        }
    }

    cout << "Array is sorted in " << totalNumberOfSwaps << " swaps." << "\n"
         << "First Element: " << a[0] << "\n"
         << "Last Element: " << a[n - 1] << "\n";
}

int main() 
{
    int n;
    cin >> n;
    
    vector<int> a(n);
    
    for(int a_i = 0; a_i < n; a_i++)
    {
       cin >> a[a_i];
    }
    
    bubbleSort(a, n);
    
    return 0;
}
 
 
