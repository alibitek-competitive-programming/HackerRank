#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_map>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int n; // number of entries in the phone book
    cin >> n;
    
    unordered_map<string, string> phoneBook;
    
    for (int i = 0; i < n; i++) 
    {
        string friendName, phoneNumber;
        cin >> friendName >> phoneNumber;
        phoneBook.insert(make_pair(friendName, phoneNumber));
    }
    
    string query;
    while (cin >> query)
    {
        unordered_map<string, string>::const_iterator foundIt = phoneBook.find(query);
        if (foundIt != phoneBook.cend()) 
        {
            cout << query << "=" << foundIt->second << '\n';
        } 
        else 
        {
            cout << "Not found" << '\n';
        }
    }
    
    return 0;
}
 
