#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

class Difference {
private:
    vector<int> elements;
  
public:
    int maximumDifference;
    
    Difference(const vector<int>& elements)
        : elements(elements)
    {}
    
    void computeDifference()
    {
        vector<int>::size_type size = elements.size();
        for (int i = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j)
            {
                auto diff = abs(elements[i] - elements[j]);
                if (diff > maximumDifference)
                    maximumDifference = diff;
            }
        }
    }
}; // End of Difference class

int main() {
    int N;
    cin >> N;
    
    vector<int> a;
    
    for (int i = 0; i < N; i++) {
        int e;
        cin >> e;
        
        a.push_back(e);
    }
    
    Difference d(a);
    
    d.computeDifference();
    
    cout << d.maximumDifference;
    
    return 0;
}
