#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cstdint>
using namespace std;

uint64_t convert(uint64_t number, uint64_t base)
{
    if (number == 0 || base == 10)
    {
        return number;
    }
    
    return (number % base) + 10 * convert(number / base, base);
}

uint64_t consecutive(uint64_t number)
{        
    if (number == 0 || number == 1)
        return number;
    
    uint64_t currentMax = 0, nextMax = 0;
    
    while (number > 0)
    {
        uint64_t lastDigit = number % 10;
    
        if (lastDigit == 1)
        {
            nextMax += 1;
        }
        else
        {
            nextMax = 0;
        }
        
        currentMax = max(currentMax, nextMax);        
        
        number /= 10;
    }    
    
    return currentMax;
}
     
int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    uint64_t n;
    cin >> n;
        
    uint64_t binary = convert(n, 2);
    uint64_t result = consecutive(binary);
    
    //cout << binary <<  " " << result << endl;
    cout << result << endl;
    
    return 0;
}
