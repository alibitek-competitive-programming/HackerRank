#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

uint64_t factorial(uint64_t n) 
{
    if (n <= 1)
        return 1;
    
    return n * factorial(n - 1);
}

uint64_t factorial_iterativ(uint64_t n) 
{
    if (n <= 1)
        return 1;
    
    uint64_t f = 2;
    
    for (int i = 3; i <= n; i++) 
    {
        f *= i;
    }
    
    return f;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    uint64_t n;
    cin >> n;
    
    cout << factorial(n);
    
    return 0;
}
 
