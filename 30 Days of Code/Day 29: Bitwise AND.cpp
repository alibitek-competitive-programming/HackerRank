#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int T;
    cin >> T; // [1, 1000]
    
    while (T-- > 0) {
        int n, // [2, 1000] 
            k; // [2, n]
        cin >> n >> k;
        
        int max = 0;
        
        for (int i = 1; i < n; i++) {
            for (int j = i + 1; j <= n; j++) {
                int r = i & j;
                
                if (r > max && r < k) {
                    max = r;
                }
            }
        }
        
        cout << max << '\n';
    }
    
    return 0;
}
