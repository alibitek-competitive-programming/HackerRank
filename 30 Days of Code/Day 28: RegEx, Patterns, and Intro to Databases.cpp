#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <regex>
#include <set>
#include <iterator>

using namespace std;

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    int n;
    cin >> n;
    
    regex emailRegex(R"([a-z.]+@gmail\.com)");
    
    multiset<string> matches;
    
    while (n-- > 0) {
        string firstName, email;
        cin >> firstName >> email;
        
        if(regex_match(email, emailRegex)) {
           matches.insert(firstName);
        }
    }
    
    copy(cbegin(matches), cend(matches), ostream_iterator<string>(cout, "\n"));
    
    return 0;
}
