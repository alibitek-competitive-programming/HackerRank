#include <bits/stdc++.h>

using namespace std;

#define MAXVAL (int)1e9

/*
 Input:
 - why how whywhat what 
   2 
   why
   what
   Output: 20
 - why how what how when how when what
    2
    what
    when
   Output: 9
 - why how whywhat why what how what when what
    3
    why
    how
    what
    Output: 12
 - 1 2 2 3
   3
   1 2 3
   Output: 7
 */

int minimumLength(string text, vector<string> keys) 
{
    int answer = MAXVAL;
    
    istringstream stream(text);
	string word;
    vector<string> words;
    
    vector<string> dup = keys;
    
	while (stream >> word)
	{
        vector<string>::iterator foundIt = find_if(dup.begin(), dup.end(), [&word](const string& keyword) { return word == keyword; });
        if ( foundIt != dup.end() )
            dup.erase(foundIt);

		words.emplace_back(word);
	}
	
	// If all the keywords aren't present in the paragraph, then print -1
	if (dup.size())
        return -1;
	
    vector<string>::size_type numberOfWords = words.size();
    vector<string>::size_type numberOfKeys = keys.size();
    
    for (vector<string>::iterator it = words.begin(); it != words.end(); ++it)
    {                
        /*if (answer != MAXVAL && i <= numberOfWords - numberOfKeys)
            break;
          */  
        dup = keys;
        
        int subStringLength = 0;
        
        string word = *it;
        //cout << "\n\ncurrent word 1: " << word << endl;
        
        vector<string>::iterator foundItx = find_if(keys.begin(), keys.end(), [&word](const string& keyword) { return word == keyword; });
        if ( foundItx == keys.end())
        {
            continue;
        }
        
        if (distance(it, words.end()) < numberOfKeys)
            break;
    
        vector<string>::iterator firstKeyword = words.end();
        
        for (vector<string>::iterator it2 = it; it2 != words.end(); ++it2)
        {
            word = *it2;
            subStringLength += word.size() + 1;
           
            //cout << "current word 2: " << word << endl;

            vector<string>::iterator foundIt = find_if(dup.begin(), dup.end(), [&word](const string& keyword) { return word == keyword; });
            if ( foundIt != dup.end() )
            {
                //cout << "found keyword new: " << word << endl;
                dup.erase(foundIt);
                
                if (firstKeyword == words.end())
                    firstKeyword = it2;
            }
            
            if (!dup.size())
            {
                subStringLength -= 1;
                //cout << "found substring: " << subStringLength << endl;
                
                /* not fast enough
                subStringLength = accumulate(firstKeyword, next(it2),
                                                distance(firstKeyword, it2),
                                                [](int a, string b) 
                                                {
                                                    return a + b.size();
                                                });*/
                answer = min(answer, subStringLength); 
                break;
            }
        }
    }
    
    if(answer == MAXVAL)
    {
        answer = -1;
    }

    return answer;
}

int main() 
{
    string text, buf;
    vector < string > keys;
    getline(cin, text);
    
    int keyWords;
    cin >> keyWords;
    
    for(int i = 0; i < keyWords; i++) 
    {
        cin >> buf;
        keys.push_back(buf);
    }
    
    cout << minimumLength(text, keys) << endl;

    return 0;
}
