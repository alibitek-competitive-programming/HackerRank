#include <bits/stdc++.h>

using namespace std;

template<typename RandomAccessIterator, typename Order>
void mergesort(RandomAccessIterator first, RandomAccessIterator last, Order order)
{
  if (last - first > 1)
  {
    RandomAccessIterator middle = first + (last - first) / 2;
    mergesort(first, middle, order);
    mergesort(middle, last, order);
    std::inplace_merge(first, middle, last, order);
  }
}
 
template<typename RandomAccessIterator>
void mergesort(RandomAccessIterator first, RandomAccessIterator last)
{
  mergesort(first, last, std::less<typename std::iterator_traits<RandomAccessIterator>::value_type>());
}

//mergesort(ar.rbegin(), ar.rend());

template<typename T>
inline bool updateMax(std::atomic<T>& atom, const T val)
{
    T atom_val = atom;
    
    if (atom_val < val)
    { 
        while (!atom.compare_exchange_weak(atom_val, val, std::memory_order_relaxed))
            ;
        
        return true;
    }
    
    return false;
}

template<typename Iterator, typename Func, typename Distance>
void chunks(Iterator begin, Iterator end, Distance k, Func f)
{
    Iterator chunk_begin;
    Iterator chunk_end;
    chunk_end = chunk_begin = begin;

    do
    {
        if(std::distance(chunk_end, end) < k)
        {
            chunk_end = end;
        }
        else
        {
            std::advance(chunk_end, k);
        }
        
        f(chunk_begin, chunk_end);
        
        chunk_begin = chunk_end;
    }
    while(std::distance(chunk_begin, end) > 0);
}

int castleTowers(int n, vector <int> ar) {
    if (!n)
        return n;
    
    std::atomic<int> max { ar[0] }, cnt { 0 };
    
    int chunkSize = round(n / 2),
        numberOfChunks = round(n / chunkSize);
        
    unsigned int num_threads = std::thread::hardware_concurrency();
    std::vector<std::thread> threads;
    threads.reserve(numberOfChunks);
    
    //cout << "max: " << max << ", chunkSize: " << chunkSize << ", numberOfChunks: " << numberOfChunks << endl;
    
    chunks(ar.begin(), ar.end(), chunkSize, [&threads, &max, &cnt](const vector<int>::iterator & chunkStart, const vector<int>::iterator & chunkEnd)
    {
        //copy(chunkStart, chunkEnd, ostream_iterator<int>(cout, "\n"));
        //cout << "chunk length1: " << std::distance(chunkStart, chunkEnd) << endl;
        
        threads.emplace_back(std::thread([](const vector<int>::iterator & chunkStart, const vector<int>::iterator & chunkEnd, std::atomic<int> & max, std::atomic<int> &cnt) 
            {
                //cout << "chunk length: " << std::distance(chunkStart, chunkEnd) << endl;
                
                for_each(chunkStart, chunkEnd, [&max, &cnt](int height)
                    {
                        //cout << "[" << std::hash<std::thread::id>()(std::this_thread::get_id()) << "]" << " ~ height: " << height << endl;
                        
                        if (updateMax(max, height)) 
                        {
                            //cout << "New max: " << max.load(std::memory_order_relaxed) << "\n";
                            cnt.store(1, std::memory_order_relaxed); 
                        }
                        else if (height == max.load(std::memory_order_relaxed))
                        {
                            //cout << "Update old max: " << max.load(std::memory_order_relaxed) << "\n";
                            cnt.fetch_add(1, std::memory_order_relaxed);
                        }
                    }
                );
            }, chunkStart, chunkEnd, std::ref(max), std::ref(cnt)
       ));
    });

    for (int i = 0; i < threads.size(); ++i) 
    {
        threads[i].join();
    }

    return cnt;
    /* Not accepted:
    1) sort(ar.rbegin(), ar.rend());
        int maxi = ar[0];
        int cnt = 1;
        for (int i = 1; i < n; i++) {
            if (maxi == ar[i]) {
                cnt += 1;
            } else {
                break;
            }
        }
        return cnt;
    
    2)
     sort(ar.rbegin(), ar.rend());
     return count(ar.begin(), ar.end(), ar[0]);
     */
    
    /*
    Accepted:
    1) return count(ar.begin(), ar.end(), *max_element(ar.begin(), ar.end()));
    
    2) 
    int cnt = 1;
    int max = ar[0];
    
    for (int i = 1; i < n; i++) {
        if (ar[i] > max) 
        {
            max = ar[i];
            cnt = 1;
        }
        else if (ar[i] == max)
        {
            cnt += 1;
        }
    }
    
    return cnt;
    */
}

int main() {
    int n;
    cin >> n; // number of towers in the city
    
    // height of each tower
    vector<int> ar(n);
    for(int ar_i = 0; ar_i < n; ar_i++){
    	cin >> ar[ar_i];
    }
    
    int result = castleTowers(n, ar);
    cout << result << endl;
    
    return 0;
}
