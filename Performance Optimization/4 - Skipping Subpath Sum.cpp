#include <bits/stdc++.h>

using namespace std;

void init()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
}

static int dummy = (init(), 0);

bool bfs(vector<vector<int>>& graph, int sourceNode, int destinationNode, vector<int>& shortestPath)
{
    if (sourceNode == destinationNode)
    {
        shortestPath.push_back(sourceNode);
        return true;
    }
    
    auto n = graph.size();
    vector<int> p(n, -1);
    vector<bool> nodesVisited(n);
    queue<int> nodesToVisit;
    
    nodesVisited[sourceNode] = true;
    
    nodesToVisit.push(sourceNode);
    
    bool foundDest = false;
    bool breakFree = false;
    
    while (!nodesToVisit.empty())
    {
        int currentVisitedNode = nodesToVisit.front();
        nodesToVisit.pop();
        
        /*
        if (currentVisitedNode == destinationNode)
        {
            foundDest = true;
            break;
        }*/
        
        for (const int & adjacentNode : graph[currentVisitedNode])
        {
            if (!nodesVisited[adjacentNode])
            {
                nodesVisited[adjacentNode] = true;
                nodesToVisit.push(adjacentNode);
                
                /*if (find(shortestPath.begin(), shortestPath.end(), adjacentNode) == shortestPath.end())
                    shortestPath.push_back(adjacentNode);
                */
                p[adjacentNode] = currentVisitedNode;
            }
            
            if (adjacentNode == destinationNode)
            {
                foundDest = true;
                breakFree = true;
                break;
            }
        }
        
        if (breakFree)
            break;
    }
    
    if (foundDest)
    {
        int i = destinationNode;
        shortestPath.push_back(i);
        
        int x = p.size();
        while (x >= 0)
        {
            int pi = p[i];
            shortestPath.push_back(pi);
            
            if (pi == sourceNode)
                break;
            
            i = pi;
            x--;
        }
    }
    
    reverse(shortestPath.begin(), shortestPath.end());
    
    return foundDest;
} 

bool dfs(vector<vector<int>>& g, int v, int p, int target, vector<int>& path) 
{
    path.push_back(v);
    
    if (v == target) 
    {
        return true;
    }
    
    for (auto u : g[v])
    {
        if (u == p) 
            continue;
        
        bool found = dfs(g, u, v, target, path);
        
        if (found) 
            return true;
    }
    
    path.pop_back();
    
    return false;
}

/* 
 * Kadane's algorithm: https://en.wikipedia.org/wiki/Maximum_subarray_problem
 */
int kadane(const vector<int>& a) 
{
    if (a.empty()) 
        return 0;
    
    int max_ending_here = max(a[0], 0);
    int max_so_far = max_ending_here;
    
    for (int i = 1; i < a.size(); ++i) 
    {
        max_ending_here = max (
                              max(0, a[i]), 
                              max_ending_here + a[i]
                         );
        
        max_so_far = max(max_so_far, max_ending_here);
    } 
    
    return max_so_far;
}

vector <int> skippingSubpathSum(int n, vector <int> c, vector < vector<int> > graph, vector <pair<int, int>> queries) 
{
    auto queriesSize = queries.size();
    
    vector<int> answers(queriesSize);
    typedef unordered_map< string, vector<int> > TCache;
    TCache dfsCache;
    
    for (int qid = 0; qid < queriesSize; ++qid) 
    {
        int u = queries[qid].first;
        int v = queries[qid].second;

        // ===============================================================================================================
        
        ostringstream os;
        os << u << v;
        string key = os.str();
        
        // Path cache
        vector<int> path;
        
        bool reversed = false;
        
        TCache::iterator foundIt = dfsCache.find(key);
        if (foundIt == dfsCache.end())
        {
            ostringstream os;
            os << v << u;
            foundIt = dfsCache.find(os.str());
            reversed = true;
        }

        if (foundIt != dfsCache.end())
        {
            path = foundIt->second;
            if (reversed)    
                reverse(path.begin(), path.end());
        }
        else
        {
            if (bfs(graph, u, v, path))
                dfsCache.insert(TCache::value_type(key, path));
            else
            {
                answers[qid] = 0;
                continue;
            }
            
            cout << "path: ";
            std::copy(path.begin(), path.end(), std::ostream_iterator<int>(std::cout, " "));
            cout << endl;
        }
        
        // ===============================================================================================================
        
        auto pathSize = path.size();
        
        if (pathSize == 1)
        {
            answers[qid] = max(0, c[ path[0] ]);
            continue;
        } 
        else if (pathSize == 0) // no path
        {
            answers[qid] = 0;
            continue;
        }

        int s1 = 0, s2 = 0;
        
        bool even_all_positive = true;
        bool even_all_negative = true;
        bool odd_all_positive = true;
        bool odd_all_negative = true;
        
        int even_sum = 0;
        int odd_sum = 0;
        
        int even_max_ending_here = 0, even_max_so_far = 0;
        int odd_max_ending_here = 0, odd_max_so_far = 0;
        
        for (int o = 0, e = 1; (o < pathSize) || (e < pathSize); o += 2, e += 2) 
        {
            // even
            if (e < pathSize)
            {
                    auto val = c[ path[e] ];
                    even_sum += val;
                    
                    if (even_all_negative && val > 0)
                        even_all_negative = false;
                    
                    if (even_all_positive && val < 0)
                        even_all_positive = false;
                    
                    even_max_ending_here = max (
                        max(0, val), 
                        even_max_ending_here + val
                    );

                    even_max_so_far = max(even_max_so_far, even_max_ending_here);
            } 
            
             // odd
            if (o < pathSize)
            {
                auto val = c[ path[o] ];
                odd_sum += val;
                
                if (odd_all_negative && val > 0)
                    odd_all_negative = false;
                
                if (odd_all_positive && val < 0)
                    odd_all_positive = false;
                
                odd_max_ending_here = max (
                    max(0, val), 
                    odd_max_ending_here + val
                );

                odd_max_so_far = max(odd_max_so_far, odd_max_ending_here);
            }
        }
        
        int answer = 0;

        if (even_all_negative)
            s1 = 0;
        else if (even_all_positive)
            s1 = even_sum;
        else
            s1 = even_max_so_far;
        
        if (odd_all_negative)
            s2 = 0;
        else if (odd_all_positive)
            s2 = odd_sum;
        else
            s2 = odd_max_so_far;
        
        answer = max(s1, s2);
        
        answers[qid] = answer;
    }
    
    return answers;
}

int main() {
    int n;
    cin >> n;
    
    vector<int> c(n);
    for(int c_i = 0; c_i < n; c_i++)
    {
       cin >> c[c_i];
    }
    
    vector<vector<int>> graph(n);
    for (int i = 0; i < n-1; ++i) 
    {
        int u, v;
        cin >> u >> v;
        graph[u].push_back(v);
        graph[v].push_back(u);
    }
    
    int q;
    cin >> q;
    
    vector<pair<int, int>> queries;
    for (int i = 0; i < q; ++i) 
    {
        int u, v;
        cin >> u >> v;
        queries.push_back(make_pair(u, v));
    }
    
    vector <int> answers = skippingSubpathSum(n, c, graph, queries);
    for (ssize_t i = 0; i < answers.size(); i++) 
    {
        cout << answers[i] << (i != answers.size() - 1 ? "\n" : "");
    }
    
    cout << endl;


    return 0;
}
