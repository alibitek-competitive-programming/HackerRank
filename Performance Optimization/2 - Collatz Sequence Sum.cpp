#include <bits/stdc++.h>

using namespace std;

int collatzSequenceLen(int n) 
{
    if (n == 0 || n == 1) 
    {
        return n;
    }
    
    // n is even
    if (n % 2 == 0) 
    {
        return 1 + collatzSequenceLen(n/2);
    }
    
    // n is odd
    return 1 + collatzSequenceLen(3*n+1);
}

int collatzSequenceSum(int T, int A, int B) 
{
    int n = 0;
    int result = 0;
    
    typedef std::unordered_map<int, int> TCache;
    TCache cacheLen;
    TCache cacheBest;
    
    // T = number of tests
    while (T--) // 3 , 2, 1
    {
        n = (A*n + B) % 5003;
        
        cacheLen.reserve(n);
        cacheBest.reserve(n);
        
        // length of the longest sequence
        int best_len = 0;
        
        // number generating the longest sequence
        int best_num = 0;
        
        TCache::const_iterator foundBestIt = cacheBest.find(n);
        if (foundBestIt != cacheBest.end())
        {
            best_num = foundBestIt->second;
        }
        else
        {
            for (int k = 0; k <= n; ++k) 
            {
                int cur_len = 0;
                
                TCache::const_iterator foundIt = cacheLen.find(k);
                
                if (foundIt != cacheLen.end())
                {
                    cur_len = foundIt->second;
                }
                else
                {
                    cur_len = collatzSequenceLen(k);
                    cacheLen.insert(std::move(TCache::value_type(k, cur_len)));
                }
                
                //cout << "k: " << k << ", len: " << cur_len << "\n";
                
                if (cur_len >= best_len) 
                {
                    best_len = cur_len;
                    best_num = k;
                }
            }

            cacheBest.insert(std::move(TCache::value_type(n, best_num)));
        }
        
        //cout << "n: " << n << ", best_num: " << best_num << "\n";
        result += best_num;
    }
    
    return result;
}

int main() 
{
    int T;
    int A;
    int B;
    cin >> T >> A >> B;
    
    int result = collatzSequenceSum(T, A, B);
    cout << result << endl;
    
    return 0;
}
