#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

struct Matrix
{       
    Matrix operator +(const Matrix& rhs)
    {            
        Matrix result;
        result.a.reserve(a.size());  
        for (auto i = 0; i < a.size(); i++)
        {            
            result.a[i].reserve(a[i].size());                        
        }
        
        for (vector<vector<int>>::size_type i = 0; i < rhs.a.size(); i++)
        {
            for (vector<int>::size_type j = 0; j < rhs.a[i].size(); j++)
            {
                result.a[i][j] = rhs.a[i][j] + a[i][j];
            }
        }
        
        return result;
    }   
    
    vector<vector<int> > a;
};

int main () {
   int cases,k;
   cin >> cases;
   for(k=0;k<cases;k++) {
      Matrix x;
      Matrix y;
      Matrix result;
      int n,m,i,j;
      cin >> n >> m;
      for(i=0;i<n;i++) {
         vector<int> b;
         int num;
         for(j=0;j<m;j++) {
            cin >> num;
            b.push_back(num);
         }
         x.a.push_back(b);
      }
      for(i=0;i<n;i++) {
         vector<int> b;
         int num;
         for(j=0;j<m;j++) {
            cin >> num;
            b.push_back(num);
         }
         y.a.push_back(b);
      }
      result = x+y;
      for(i=0;i<n;i++) {
         for(j=0;j<m;j++) {
            cout << result.a[i][j] << " ";
         }
         cout << endl;
      }
   }  
   return 0;
} 
